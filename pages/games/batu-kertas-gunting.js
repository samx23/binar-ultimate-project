import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import db from "../../services/firebase-storage";
import PlayGames from "../../utils/RockPaperScrissor";
import ButtonChoice from "../../components/ButtonChoice";
import styles from "../../styles/Game_1.module.css";

const RockPaperScrissor = ({ initialReduxState }) => {
  const router = useRouter();
  const [{ user }] = initialReduxState;

  const [player, setPlayer] = useState({
    name: "Player One",
  });

  useEffect(() => !user && router.push("/"), [user]);

  useEffect(() => {
    db.collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => {
        setPlayer(doc.data());
        PlayGames(doc.data());
      });
  }, [user.uid]);

  return (
    <div className={styles.gameBody}>
      <header>
        <nav className={styles.gameNavigation}>
          <div className={styles.back_button}>
            <Link href="/games/batu-kertas-gunting" className="d-flex">
              <ion-icon name="arrow-back-outline"></ion-icon>
            </Link>
          </div>
          <div className={styles.title}>
            <div className={styles.title_logo}>
              <img
                loading="lazy"
                width="80px"
                height="80px"
                src="/images/logo.png"
                alt="game's logo"
              />
            </div>
            <div className={styles.title_text}>
              <h1 id="game" className="text-uppercase">
                Games Room
              </h1>
            </div>
          </div>
        </nav>
      </header>

      <div className={styles.container}>
        <main className={styles.main}>
          <div className={styles.player}>
            <div className={styles.user_title}>
              <h2
                className="text-uppercase"
                id="player"
                data-id="{ player.id }"
                data-username="{ player.username }"
              >
                {player.name}
              </h2>
            </div>
            <div className={styles.user_choice}>
              <ButtonChoice choice="batu" />
              <ButtonChoice choice="kertas" />
              <ButtonChoice choice="gunting" />
            </div>
          </div>
          <div id="vs_result" className={styles.versus}></div>
          <div className={styles.player}>
            <div className={styles.user_title}>
              <h2 id="player-2" data-id="" data-username="COM">
                COM
              </h2>
            </div>
            <div className={styles.user_choice}>
              <ButtonChoice choice="batu" />
              <ButtonChoice choice="kertas" />
              <ButtonChoice choice="gunting" />
            </div>
          </div>

          <div className={styles.reset}>
            <button id="reset" type="reset">
              <img
                loading="lazy"
                width="68px"
                height="68px"
                src="/images/refresh.png"
                alt="reset button"
              />
            </button>
          </div>
        </main>
      </div>
    </div>
  );
};

export default RockPaperScrissor;

export const getStaticProps = async (context) => {
  return {
    props: { initialReduxState: Store.getState() },
  };
};
