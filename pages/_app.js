import Script from "next/script";
import { Provider } from "react-redux";
import Store from "../redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import { useEffect } from "react";
import { auth, onAuthStateChanged } from "../services/firebase-auth";
import setSession from "../hooks/setSession";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    const listener = window
      ? onAuthStateChanged(auth, (authUser) => {
          setSession("user", JSON.stringify(authUser));
          Store.dispatch({
            type: "SET_USER",
            payload: authUser,
          });
        })
      : null;

    return () => listener;
  }, [Store.dispatch]);

  return (
    <Provider store={Store}>
      <Component {...pageProps} />

      <Script
        type="module"
        src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        strategy="lazyOnload"
      />
      <Script
        nomodule
        src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        strategy="lazyOnload"
      />
    </Provider>
  );
}

export default MyApp;
