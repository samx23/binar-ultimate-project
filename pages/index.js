import { useEffect, useState } from "react";
import Layout_main from "../components/layout/main";
import Store from "../redux";
import styles from "../styles/Home.module.css";

export default function Home({ initialReduxState }) {
  const [state, setState] = useState(initialReduxState);

  // Ambil dan logging data dari redux
  useEffect(() => {
    console.log(state);
  }, []);

  return (
    // Layout halaman home
    <Layout_main className={styles.container}>
      <main className={styles.main}>
        <h1>Halaman HOME</h1>
      </main>
    </Layout_main>
  );
}

// Inisiasi props untuk SSR
export const getStaticProps = async (context) => {
  return {
    props: { initialReduxState: Store.getState() },
  };
};
