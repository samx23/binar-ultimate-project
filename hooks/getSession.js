const getSession = (name) =>
  typeof window !== "undefined" ? sessionStorage.getItem(name) : null;

export default getSession;
