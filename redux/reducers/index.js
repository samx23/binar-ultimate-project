import { combineReducers } from "redux";
import persons from "./person";
import users from "./user";

const reducer = combineReducers({
  persons,
  users,
});

export default reducer;
