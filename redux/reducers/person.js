// Untuk test saja
const initialState = [
  {
    id: 1,
    name: "Sami Kalammallah",
    address: "Bandung",
    phone: "23151231235",
    photo: "google.com",
  },
];

export default function persons(state = initialState, action) {
  switch (action.type) {
    case "ADD":
      return [...state, action.payload];
    default:
      return state;
  }
}
