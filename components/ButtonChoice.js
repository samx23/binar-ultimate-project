import { useEffect, useRef, useState } from "react";

const ButtonChoice = ({ user, choice }) => {
  const [picks, setPicks] = useState(null);
  const ref = useRef(choice);

  useEffect(() => {
    setPicks(choice);
  }, [choice]);

  const handleClicks = (e) => {
    if (ref.current.id !== `${choice}-com`) {
      console.log("Clicked", ref.current.dataset);
    }
  };

  return (
    <button
      className={`choice ${choice}`}
      onClick={handleClicks}
      ref={ref}
      data-choice={choice}
      data-user={user}
    >
      <img
        loading="lazy"
        width="128px"
        height="128px"
        src={`/images/${picks}.png`}
        alt={picks}
      />
    </button>
  );
};

export default ButtonChoice;
