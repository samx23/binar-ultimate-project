import Header from "../Header";

const Layout_main = ({ children }) => {
  return (
    <>
      <Header />
      {children}
    </>
  );
};

export default Layout_main;
