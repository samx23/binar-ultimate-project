import Store from "../redux";
import db from "../services/firebase-storage";

const context = Store.getState();
const { user } = context;

const collection = () => db.collection("users").doc(user.uid);

// user is destructuring from userCredential
const createUser = (name, email) =>
  collection.set({
    uid: user.uid,
    name,
    avatar: null,
    authProvider: "local",
    email,
    win: 0,
    lose: 0,
    gameHistory: null,
    achievement: null,
    score: 0,
  });

const getUser = () => collection.get();

export { collection, createUser, getUser };
